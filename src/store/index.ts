import React, { createContext } from 'react';

import { AppStore } from './AppStore';

class RootStore {
    public readonly appStore: AppStore;

    constructor() {
        this.appStore = new AppStore();
    }
}

export const rootStore = new RootStore();

export const RootStoreContext = createContext<RootStore>(rootStore);

export const useStore = () => React.useContext(RootStoreContext);
