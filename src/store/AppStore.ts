import { observable, action, makeObservable, computed } from 'mobx';

import { ISearchItem } from '../utils/api';

interface IPagination {
    count: number;
    activePage: number;
}

export const MAX_PAGE_COUNT = 10;

export class AppStore {
    constructor() {
        makeObservable(this);
    }

    @observable
    public searchValue: string = '';

    @observable
    public searchList: ISearchItem[] = [];

    @observable
    public pagination: IPagination = {
        count: 0,
        activePage: 1,
    };

    @computed
    public get paginatedList() {
        const { activePage } = this.pagination;
        const start = (activePage - 1) * MAX_PAGE_COUNT;
        const end = activePage * MAX_PAGE_COUNT;

        return this.searchList.slice(start, end);
    }

    @action.bound
    public changeSearch(value: string): void {
        this.searchValue = value;
    }

    @action.bound
    public changeSearchList(newList: ISearchItem[]): void {
        this.changePagination({ activePage: 1, count: newList.length });
        this.searchList = newList;
    }

    @action.bound
    public changePagination(pagination: IPagination): void {
        this.pagination = pagination;
    }
}
