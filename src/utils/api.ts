import axios from 'axios';

import { tryParse } from './tools';

const API_URL = 'https://clinicaltables.nlm.nih.gov/api/npi_idv/v3/search';

export interface ISearchItem {
    name: string;
    npi: string;
    type: string;
    practiceAddress: string;
}

export interface ISearchData {
    data: ISearchItem[];
    totalCount: number;
}

export type TDetails = Record<string, string>;

export async function getSearchList(terms: string): Promise<ISearchData> {
    try {
        const res = await axios.get(`${API_URL}?terms=${terms}&maxList=`);
        const [totalCount, , , data] = res.data;

        return {
            totalCount,
            data: data.map(([name, npi, type, practiceAddress]: [string, string, string, string]) => ({
                name,
                npi,
                type,
                practiceAddress,
            })),
        };
    } catch (error) {
        console.error(error);
    }

    return {
        totalCount: 0,
        data: [],
    };
}

export async function getSearchItem(npi: string): Promise<null | TDetails> {
    try {
        const dfParams = [
            'provider_type',
            'gender',
            'name',
            'licenses',
            'addr_practice',
            'addr_mailing',
            'name_other',
            'other_ids',
            'misc',
        ];
        const res = await axios.get(`${API_URL}?terms=${npi}&df=${dfParams.join(',')}`);
        const [, , , arr] = res.data;
        const [data] = arr as [string[]];

        const details: TDetails = {};
        data.forEach((it, index) => {
            const fieldName = dfParams[index];
            const parsed = tryParse(it);

            if (typeof parsed === 'object') {
                Object.entries(parsed).forEach(([subFieldName, subFieldValue]) => {
                    if (typeof subFieldValue === 'object') {
                        Object.entries(subFieldValue).forEach(([thirdFieldName, thirdFieldValue]) => {
                            details[`${fieldName}.${subFieldName}.${thirdFieldName}`] = JSON.stringify(thirdFieldValue);
                        });
                    } else {
                        details[`${fieldName}.${subFieldName}`] = subFieldValue;
                    }
                });
            } else {
                details[fieldName] = it;
            }
        });

        return details;
    } catch (error) {
        console.error(error);
    }

    return null;
}
