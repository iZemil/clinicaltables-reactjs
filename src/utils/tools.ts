export const tryParse = (value: string): string | Object => {
    if (value === '') {
        return value;
    }

    let parsed = value;
    try {
        parsed = JSON.parse(value);
    } catch (error) {
        // console.error('Parse error: Invalid JSON', error);
    }

    return parsed;
};
