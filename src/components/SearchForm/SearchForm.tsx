import React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Button, Header, Input, Pagination, Table } from 'semantic-ui-react';

import { IRoute } from '../../routes';
import { getSearchList } from '../../utils/api';
import { useStore } from '../../store';
import { MAX_PAGE_COUNT } from '../../store/AppStore';

export const SearchForm = observer((props: IRoute) => {
    const { appStore } = useStore();
    const { pagination, searchValue, paginatedList, changePagination, changeSearch, changeSearchList } = appStore;
    const { activePage, count } = pagination;

    const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { value } = e.target;

        changeSearch(value);
    };
    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
        e.preventDefault();

        const searchData = await getSearchList(searchValue);

        if (searchData) {
            const { data } = searchData;
            changeSearchList(data);
        }
    };

    return (
        <div>
            <Header as="h1">NPI records searcher</Header>

            <form onSubmit={handleSubmit}>
                <Input
                    type="text"
                    placeholder="Provider name or NPI"
                    value={searchValue}
                    onChange={handleChangeSearch}
                    action
                >
                    <input />
                    <Button type="submit">Search</Button>
                </Input>
            </form>

            <Table celled selectable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>NPI</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Type</Table.HeaderCell>
                        <Table.HeaderCell>Practice Address</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {paginatedList.map((it) => (
                        <Table.Row key={it.npi}>
                            <Table.Cell>
                                <Link to={`/details/${it.npi}`}>{it.npi}</Link>
                            </Table.Cell>
                            <Table.Cell>{it.name}</Table.Cell>
                            <Table.Cell>{it.type}</Table.Cell>
                            <Table.Cell>{it.practiceAddress}</Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>

            {count > MAX_PAGE_COUNT && (
                <Pagination
                    activePage={activePage}
                    totalPages={Math.ceil(count / MAX_PAGE_COUNT)}
                    onPageChange={(e, data) => {
                        const { activePage } = data;

                        changePagination({ ...pagination, activePage: Number(activePage) });
                    }}
                />
            )}
        </div>
    );
});
