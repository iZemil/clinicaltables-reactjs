import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb, Header, Table } from 'semantic-ui-react';

import { ROUTES, IRoute } from '../../routes';
import { getSearchItem, TDetails } from '../../utils/api';

// TODO: Sometimes backed json is broken for licenses row
const brokenJSONStyle = { whiteSpace: 'break-spaces' };

export function DetailsForm(props: IRoute) {
    const {
        match: {
            params: { npi },
        },
    } = props;
    const [details, changeDetails] = useState<TDetails>({});

    async function fetchDetails(npi: string): Promise<void> {
        const data = await getSearchItem(npi);

        if (data) {
            changeDetails(data);
        }
    }
    useEffect(() => {
        fetchDetails(npi);
    }, [npi]);

    return (
        <div>
            <Header as="h1">Details page</Header>

            <Breadcrumb>
                <Breadcrumb.Section link>
                    <Link to={ROUTES.home.path}>Searcher</Link>
                </Breadcrumb.Section>
                <Breadcrumb.Divider />
                <Breadcrumb.Section>Details</Breadcrumb.Section>
                <Breadcrumb.Divider />
                <Breadcrumb.Section active>NPI: {npi}</Breadcrumb.Section>
            </Breadcrumb>

            <Table singleLine selectable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Field</Table.HeaderCell>
                        <Table.HeaderCell>Value</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {Object.entries(details).map(([fieldName, value]) => (
                        <Table.Row key={fieldName}>
                            <Table.Cell>{fieldName}</Table.Cell>
                            <Table.Cell style={brokenJSONStyle}>{value}</Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        </div>
    );
}
