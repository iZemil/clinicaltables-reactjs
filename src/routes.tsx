import React from 'react';
import { RouteProps } from 'react-router-dom';

import { SearchForm } from './components/SearchForm';
import { DetailsForm } from './components/DetailsForm';

type TPages = 'home' | 'details';

export interface IRoute extends RouteProps {
    path: string;
    match?: any;
}

type TRoutes = Record<TPages, IRoute>;

export const ROUTES: TRoutes = {
    home: {
        path: '/',
        exact: true,
        component: (props: IRoute) => <SearchForm {...props} />,
    },
    details: {
        path: '/details/:npi',
        component: (props: IRoute) => <DetailsForm {...props} />,
    },
};

export const routes = Object.values(ROUTES);
