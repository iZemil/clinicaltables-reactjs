import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { routes } from './routes';
import { rootStore, RootStoreContext } from './store';

function App() {
    return (
        <RootStoreContext.Provider value={rootStore}>
            <div className="App">
                <Router>
                    <Switch>
                        {routes.map((it) => (
                            <Route key={it.path} path={it.path} exact={Boolean(it.exact)}>
                                {it.component}
                            </Route>
                        ))}
                    </Switch>
                </Router>
            </div>
        </RootStoreContext.Provider>
    );
}

export default App;
