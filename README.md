# clinicaltables-reactjs

## Test Task

We have public API: https://clinicaltables.nlm.nih.gov/apidoc/npi_idv/v3/doc.html#params

Create simple reactjs web application with 2 forms:

1. Search form where you can enter name and search data and see found results. Show 10 found items on page
2. Details form - all fields from found item on second page in html table (full list of fields you can see in NPI Field Descriptions)

## Environment

1. install nodejs
2. install npm dependencies `npm i`

## Commands

-   run dev server `npm start`
-   build production `npm run build`
